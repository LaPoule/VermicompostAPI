﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Vermicompost.Models;
using VermiCompost;

namespace Vermicompost.Controllers.Api
{
    public class VermiController: ApiController
    {
        public List<VermiRecord> GetVermi(int id)
        {
            var databaseManager = new DatabaseManager();
            List<VermiRecord> vermiRecords = databaseManager.getLastPayloads(id);

            return vermiRecords;
        }

        public IHttpActionResult PostVermi(SensorData model)
        {
            var databaseManager = new DatabaseManager();
            databaseManager.AddPayload(model);
            return Ok();
        }
    }
}