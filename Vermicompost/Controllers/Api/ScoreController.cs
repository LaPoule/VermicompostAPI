﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using VermiCompost;

namespace Vermicompost.Controllers.Api
{
    public class ScoreController : ApiController
    {
        public double GetScore(int id)
        {
            var databaseManager = new DatabaseManager();
            List<VermiRecord> vermiRecordsForCurrentCompost = databaseManager.getPayloads(id);

            double score = 0;
            foreach (var record in vermiRecordsForCurrentCompost)
            {
                if (record.SensorId == "temperature_sensor_value")
                {
                    float currentTemp = float.Parse(record.SensorValue, CultureInfo.InvariantCulture.NumberFormat);
                    double minOptimal = 15;
                    double maxOptimal = 35;

                    if (minOptimal < currentTemp && maxOptimal > currentTemp)
                    {
                        score += 1.1;
                    }
                    else
                    {
                        score -= 0.4;
                    }
                }

                if (record.SensorId == "humidity_value")
                {
                    float currentHumidity = float.Parse(record.SensorValue, CultureInfo.InvariantCulture.NumberFormat);
                    float minOptimal = 25;
                    float maxOptimal = 40;

                    if (minOptimal < currentHumidity & maxOptimal > currentHumidity)
                    {
                        score += 0.75;
                    }
                    else
                    {
                        score -= 0.35;
                    }
                }                
            }
            return score;
        }
    }
}