﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VermiCompost;

namespace Vermicompost.Controllers.Api
{
    public class CompostController : ApiController
    {
        public List<Compost> GetCompost()
        {
            var databaseManager = new DatabaseManager();
            return databaseManager.GetAllComposts();
        }
    }
}