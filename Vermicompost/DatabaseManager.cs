﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vermicompost.Controllers.Api;
using Vermicompost.Models;

namespace VermiCompost
{
    public class DatabaseManager
    {
        SqlConnectionStringBuilder cb;

        public DatabaseManager()
        {
            cb = new SqlConnectionStringBuilder();
            cb.DataSource = "vermicompost.database.windows.net";
            cb.UserID = "Vermicompost-admin";
            cb.Password = "H@ck1nthewoods";
            cb.InitialCatalog = "Vermicompost";
        }

        public List<Compost> GetAllComposts()
        {
            List<Compost> allCompost = new List<Compost>();

            try
            {
                using (var connection = new SqlConnection(cb.ConnectionString))
                {
                    connection.Open();

                    string query = "SELECT * FROM COMPOST";
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Compost compost= new Compost();
                            compost.Id = reader[0].ToString();
                            compost.latitude = reader[1].ToString();
                            compost.longitude= reader[2].ToString();
                            allCompost.Add(compost);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Do nothing
            }

            return allCompost;
        }

        internal List<VermiRecord> getPayloads(int compostId)
        {
            List<VermiRecord> allPayloadForCompost = new List<VermiRecord>();

            try
            {
                using (var connection = new SqlConnection(cb.ConnectionString))
                {
                    connection.Open();

                    string query = string.Format("SELECT * FROM SensorData WHERE CompostID = {0}", compostId);
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            VermiRecord vermiRecord = new VermiRecord();
                            vermiRecord.Id = reader[0].ToString();
                            vermiRecord.SensorId= reader[1].ToString();
                            vermiRecord.SensorValue = reader[2].ToString();
                            vermiRecord.Dts = reader[3].ToString();
                            allPayloadForCompost.Add(vermiRecord);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Do nothing
            }

            return allPayloadForCompost;
        }

        public void ExecuteNonQuery(string query)
        {
            try
            {
                using (var connection = new SqlConnection(cb.ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        internal void AddPayload(SensorData payload)
        {
            string SensorName = "UNKOWN SOURCE";
            string SensorValue = "";

            if (payload.temperature_sensor_value.HasValue)
            {
                SensorName = "temperature_sensor_value";
                SensorValue = payload.temperature_sensor_value.Value.ToString();
            }

            if (payload.air_quality_value.HasValue)
            {
                SensorName = "air_quality_value";
                SensorValue = payload.air_quality_value.Value.ToString();
            }

            if (payload.pressure_value.HasValue)
            {
                SensorName = "pressure_value";
                SensorValue = payload.pressure_value.Value.ToString();
            }

            if (payload.humidity_value.HasValue)
            {
                SensorName = "humidity_value";
                SensorValue = payload.humidity_value.Value.ToString();
            }

            int CompostID = 1;
            string dts= payload.stream_value_time.ToString();

            string query = string.Format("INSERT INTO SensorData values({0},'{1}','{2}','{3}')",CompostID,SensorName,SensorValue,dts);

            ExecuteNonQuery(query);
        }

        internal List<VermiRecord> getLastPayloads(int compostId)
        {
            List<VermiRecord> allPayloadForCompost = new List<VermiRecord>();
            List<VermiRecord> TemperaturePayload = new List<VermiRecord>();
            List<VermiRecord> HumidityPayload = new List<VermiRecord>();
            List<VermiRecord> PressurePayload = new List<VermiRecord>();
            List<VermiRecord> AirPayload = new List<VermiRecord>();

            try
            {
                using (var connection = new SqlConnection(cb.ConnectionString))
                {
                    connection.Open();

                    string query = string.Format("SELECT * FROM SensorData WHERE CompostID = {0}", compostId);
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            VermiRecord vermiRecord = new VermiRecord();
                            vermiRecord.Id = reader[0].ToString();
                            vermiRecord.SensorId = reader[1].ToString();
                            vermiRecord.SensorValue = reader[2].ToString();
                            vermiRecord.Dts = reader[3].ToString();
                            if (vermiRecord.SensorId == "temperature_sensor_value")
                                TemperaturePayload.Add(vermiRecord);

                            if (vermiRecord.SensorId == "humidity_value")
                                HumidityPayload.Add(vermiRecord);

                            if (vermiRecord.SensorId == "air_quality_value")
                                AirPayload.Add(vermiRecord);

                            if (vermiRecord.SensorId == "pressure_value")
                                PressurePayload.Add(vermiRecord);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Do nothing
            }

            VermiRecord lastTempRecord = TemperaturePayload.First();
            foreach (var vermi in TemperaturePayload)
            {
                if (Int64.Parse(vermi.Dts) > Int64.Parse(lastTempRecord.Dts))
                {
                    lastTempRecord = vermi;
                }
            }

            VermiRecord lastHumidityRecord = HumidityPayload.First();
            foreach (var vermi in HumidityPayload)
            {
                if (Int64.Parse(vermi.Dts) > Int64.Parse(lastHumidityRecord.Dts))
                {
                    lastHumidityRecord = vermi;
                }
            }

            VermiRecord lastPressureRecord = PressurePayload.First();
            foreach (var vermi in TemperaturePayload)
            {
                if (Int64.Parse(vermi.Dts) > Int64.Parse(lastPressureRecord.Dts))
                {
                    lastPressureRecord = vermi;
                }
            }

            VermiRecord lastAirRecord = AirPayload.First();
            foreach (var vermi in AirPayload)
            {
                if (Int64.Parse(vermi.Dts) > Int64.Parse(lastAirRecord.Dts))
                {
                    lastAirRecord = vermi;
                }
            }

            List<VermiRecord> LastPayloads = new List<VermiRecord>();
            LastPayloads.Add(lastTempRecord);
            LastPayloads.Add(lastHumidityRecord);
            LastPayloads.Add(lastPressureRecord);
            LastPayloads.Add(lastAirRecord);

            return LastPayloads;
        }
    }

    public class Compost
    {
        public string Id { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}
