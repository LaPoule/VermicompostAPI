﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vermicompost.Models
{
    public class SensorData
    {
        public long stream_value_time { get; set; }
        public float? air_quality_value { get; set; }
        public float? pressure_value { get; set; }
        public float? humidity_value { get; set; }
        public float? temperature_sensor_value { get; set; }
    }


}