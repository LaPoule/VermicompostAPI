﻿using Newtonsoft.Json;

namespace Vermicompost.Controllers.Api
{
    public class VermiRecord
    {
        public string Id { get; set; }
        public string SensorId { get; set; }
        public string SensorValue { get; set; }
        [JsonProperty("dts")]
        public string Dts { get; set; }
    }
}